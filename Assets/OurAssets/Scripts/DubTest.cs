using UnityEngine;
using System.Collections;

public class DubTest : MonoBehaviour 
{
	public new AudioSource audio;
	public int size;
	public bool useLine;
	public LineRenderer line;
	public GameObject cube;
	public GameObject[] bars;
	public float width;
	public float gap;
	float[] samples;
	public float yScale;
	public float yMin = 0.4f;
	public float sleepSpot = 0.0f;
	
	private Vector3[] linePoints;
	
	// Use this for initialization
	void Start () 
	{
		samples = new float[size];
		if(useLine)
		{
			line.SetVertexCount(size);
			linePoints = new Vector3[size];
		}
		else
		{
			GameObject.Destroy(line);
			bars = new GameObject[size];
		}
		
		if(audio==null)
		{
			audio = gameObject.GetComponent<AudioSource>();
		}
		
		initializeBars();
	}
	
	void initializeBars()
	{
		float startX = -(gap+width)*size/2f + gameObject.transform.position.x;
		float y, z;
		
		y = gameObject.transform.position.y;
		z = gameObject.transform.position.z;
		
		print ("Z: " + z + " Y: " + y);
		
		if(!useLine)
		{
			cube.transform.localScale = new Vector3(width, 1, 1);
			
			for(int i=0; i<size; i++)
			{
				bars[i] = Instantiate(cube) as GameObject;
				
				bars[i].transform.parent = transform;
				bars[i].transform.localPosition = new Vector3(startX, 0, 0);
				
				startX+=gap+width;
			}
		}
		else
		{
			for(int i=0; i<size; i++)
			{
				linePoints[i] = new Vector3(startX, 0, 0);
				
				line.SetPosition(i, linePoints[i]);
				
				startX+=gap+width;
			}
		}
	}
	
	
	// Update is called once per frame
	void Update () 
	{
		getAudioSamples();
		distortWave();
	}
	
	void getAudioSamples()
	{
		audio.GetOutputData(samples, 0);
	}
	
	private void distortWave()
	{
		if(useLine)
		{
			distortLine();
		}
		else
			distortBars();
	}
	
	private void distortLine()
	{
		for(int i=1; i<size; i+=2)
		{
			Vector3 scale = gameObject.transform.localScale;
			
			if(samples[i]*yScale>yMin)
			{
				scale.z = Mathf.Log(samples[i]*yScale);
			}
			else
			{
				if(scale.z>yMin)
				{
					scale.z *=sleepSpot;
				}
				else
				{
					scale.z = yMin;
				}
			}
			
			//bars[i].transform.localScale = scale;
			Vector3 newPoint = linePoints[i];
			newPoint.z += scale.z;
			line.SetPosition(i, newPoint);
		}
	}
	
	private void distortBars()
	{
		for(int i=0; i<size; i++)
		{
			Vector3 scale = bars[i].transform.localScale;
			if(samples[i]*yScale>yMin)
			{
				scale.z = Mathf.Log(samples[i]*yScale);
			}
			else
			{
				if(scale.z>yMin)
				{
					scale.z *=0.8f;
				}
				else
				{
					scale.z = yMin;
				}
			}
			
			bars[i].transform.localScale = scale;
		}
	}
}

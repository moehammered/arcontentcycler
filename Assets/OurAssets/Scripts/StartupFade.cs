using UnityEngine;
using System.Collections;

public class StartupFade : MonoBehaviour {
	
	public float fadeTimer = 3.0f;
	public GameObject nextToActivate;
	private Texture2D matTexture;
	private Color[] pixels;
	private float timer = 0;
	
	public float transDelta;
	public float transparency = 0;
	// Use this for initialization
	void Start () 
	{
		matTexture = new Texture2D(256, 256);
		gameObject.renderer.material.mainTexture = matTexture;
		
		pixels = matTexture.GetPixels();
		
		for(int y = 0; y < matTexture.height;++y)
		{
			for(int x = 0; x < matTexture.width; ++x)
			{
				pixels[y*matTexture.width + x].a = 0;
			}
		}
		
		matTexture.SetPixels(pixels);
		matTexture.Apply();
		
		transDelta = 1.0f/fadeTimer;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(transparency < 1.0f)
		{
			timer += Time.deltaTime;
			
			//if(timer >= transDelta)
			{
				timer = 0;
				transparency+= transDelta * Time.deltaTime;
				
				for(int y = 0; y < matTexture.height;++y)
				{
					for(int x = 0; x < matTexture.width; ++x)
					{
						pixels[y*matTexture.width + x].a = transparency;
					}
				}
				
				matTexture.SetPixels(pixels);
				matTexture.Apply();
			}
		}
		else
		{
			if(!audio.isPlaying)
				nextToActivate.SetActive(true);
		}
	}
}

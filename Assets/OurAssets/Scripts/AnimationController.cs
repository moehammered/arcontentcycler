using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour {
    
    public string[] animationNames;
	public bool[] animationsEnded;
	public bool loopAnim = true;
	public bool repeatAllAnimations;
	public bool animEnded = false;
    private Animation animations;
//    private bool blended = false;
	private int currentAnimIdx;
    private string currentAnimation;
    // Use this for initialization
	
	void OnEnable()
	{
		animationsEnded = new bool[animationNames.Length];
	}
	
    void Start () {
        animations = gameObject.GetComponent<Animation>();
		
		currentAnimIdx = 0;
        currentAnimation = animationNames[0];
		
        animations.Play(currentAnimation);
		animEnded = false;
    }
    
    // Update is called once per frame
    void Update () {
		checkAnimations();	
		animEnded = !animations.isPlaying;
    }
	
	private void checkAnimations()
	{
		if(repeatAllAnimations)
		{
			if(!animations.isPlaying)
			{
				cycleAnimation();
			}
		}
		else
		{
			if(currentAnimIdx != animationNames.Length - 1)
			{
				if(!animations.isPlaying)
				{
					cycleAnimation();
				}
			}
		}
	}
    
	private void cycleAnimation()
	{
		animationsEnded[currentAnimIdx] = true;
		
		currentAnimIdx++;
		
		if(currentAnimIdx == animationNames.Length)
		{
			currentAnimIdx = 0;
		}
		
		currentAnimation = animationNames[currentAnimIdx];
		animations.Play(currentAnimation);
	}
	
    private void blendAnimations(string animation)
    {
        currentAnimation = animation;
        animations.CrossFade(animation);
    }
}
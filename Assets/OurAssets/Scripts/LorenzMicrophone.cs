using UnityEngine;
using System.Collections;

public class LorenzMicrophone : MicrophoneStreamer
{
	public int pointCount = 10000;
	private LineRenderer line;
	public float x1, y1, z1, a1, b1, c1;
	public float x, y, z, a, b, c;
	public float scale = 1;
	public Material mat;
	public Color colour1, colour2;
	
	public int numberLeft, numberRight;
	
	protected override void Start()
	{
		base.Start();
		line = gameObject.GetComponent<LineRenderer>();
		line.SetVertexCount(pointCount);
		line.material = mat;
		line.SetColors(colour1, colour2);
	}
	
	protected override void FixedUpdate()
	{
		base.FixedUpdate();
		Lorenz();
		
		//micAudio.GetOutputData(numberLeft, 0);
		//micAudio.GetOutputData(numberRight, 0);
	}
	
	protected virtual void Lorenz()
	{
		float tDelta = 10f/pointCount;
		x = x1;
		y = y1;
		z = z1;
		
		b = (numberLeft+numberRight)*50+b1;
		
		float xNew, yNew, zNew;
		
		for(int i=0; i<pointCount; i++)
		{
			xNew = x + a1*(y-x)*tDelta;
			yNew = y + (x*(b-z)-y1)*tDelta;
			zNew = z+ (x*y-c1*z)*tDelta;
			
			Vector3 pos = new Vector3(xNew, yNew, zNew);
			line.SetPosition(i, pos*scale);
			
			x = xNew;
			y = yNew;
			z = zNew;
		}
	}
	
}

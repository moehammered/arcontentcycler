using UnityEngine;
using System.Collections;

public class OffButton : MonoBehaviour {
	
	public Camera mainCam;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.touchCount > 0)
		{
			Touch touch = Input.touches[0];
			
			if(touchedOffButton(touch.position))
			{
				Application.LoadLevel("TurnOff");
			}
		}
		if(Input.GetMouseButton(0))
		{
			if(touchedOffButton(Input.mousePosition))
			{
				Application.LoadLevel("TurnOff");
			}
		}
	}
	
	private bool touchedOffButton(Vector2 touchPos)
	{
		Ray ray = mainCam.ScreenPointToRay(touchPos);
		
		RaycastHit info;
		
		if(Physics.Raycast(ray, out info))
		{
			if(info.collider.gameObject.tag == "Finish")
			{
				return true;
			}
		}
		
		return false;
	}
}

using UnityEngine;
using System.Collections;

public class MicrophoneStreamer : MonoBehaviour 
{
	
	public AudioSource micAudio;
	public string[] microphoneName;
	public int targetMic = 0;
	public int recordingLength = 1;
	public bool useDefaultMic = false, loopRecording = true;
	public int minFrequency, maxFrequency;
	
	private int numberOfMics;
	private int readHead = 0, writeHead = 0;

	// Use this for initialization
	protected virtual void Start ()
	{
		micAudio = audio;
		storeMicInfo(); //Stores all available mic devices. For now only first is used.
		
		streamMicrophoneAudio(); //Start recording with microphone.
		
		print(microphoneName[targetMic]);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	
	//Called at fixed frame intervals
	protected virtual void FixedUpdate()
	{
//		print ("Flushing Audio to clip");
//		if(useDefaultMic)
//			print("Audio Recording: " + Microphone.IsRecording(null));
//		else
//			print("Audio Recording: " + Microphone.IsRecording(microphoneName[targetMic]));
//		
//		print ("Audio Playing: " + micAudio.isPlaying);
		flushMicAudio(); //Get microphone data after fixed interval to ensure it has time to record audio.
	}
	
	void storeMicInfo()
	{
		numberOfMics = Microphone.devices.Length; //Store number of mics.
		
		microphoneName = new string[numberOfMics]; //Create an array to store device names.
		
		for(int i = 0; i < numberOfMics; ++i)
		{
			microphoneName[i] = Microphone.devices[i];//Store all available microphones.
			Microphone.GetDeviceCaps(microphoneName[i], out minFrequency, out maxFrequency);
			
			if( minFrequency == 0 && maxFrequency == 0)
			{
				minFrequency = 44100;
				maxFrequency = 44100;
			}
		}
	}
	
	void streamMicrophoneAudio() //Called once to setup microphone and audio source.
	{
		if(useDefaultMic)
		{
			//print ( "Recording with microphone" );
			micAudio.clip = Microphone.Start(null, loopRecording, recordingLength, maxFrequency); //Null = default device
		}
		else if(!Microphone.IsRecording(microphoneName[targetMic])) //If the mic isn't recording yet.
		{
			//print ( "Recording with microphone: " + microphoneName[targetMic] );
			//Store the microphone recording into the audio source's clip.
			//Arguements = Device name, loop recording?, length of recording, sampling rate	
			micAudio.clip = Microphone.Start(microphoneName[targetMic], loopRecording, recordingLength, maxFrequency);
		}
		
		//This is because the mic 'restarts' it doesn't make the clip longer.
		//But playing the audio will reach the end and stop whilst the mic is set to loop back and re-record.
		micAudio.loop = true; //Makes sure the audio doesn't stop playing of the visualiser!
		micAudio.Play();
	}
	
	//As seen on Unity forum by user 'Pi_3.14' last post on page
	//http://forum.unity3d.com/threads/145686-Real-time-Audio-from-microphone?p=1156920&viewfull=1#post1156920
	protected virtual void flushMicAudio() //Get new data recorded from microphone and make it available.
	{
		if(useDefaultMic)
		{
			writeHead = Microphone.GetPosition(null);
		}
		else
		{
			writeHead = Microphone.GetPosition(microphoneName[targetMic]);
		}
		
		//Formula -- (NumberOfSamples + WriteHead - Readhead) % NumberOfSamples;
		//This will say how much sample data to retrieve from the clip recorded by the mic.
		//NumberOfSamples is obtained from audioSource.clip.samples.
		int sampleAmount = (micAudio.clip.samples + writeHead - readHead) % micAudio.clip.samples;
		
		//This is the slowest part of this function. Might not impact performance. Testing required.
		float[] soundData = new float[sampleAmount];
		
		//The formula above makes it so the readhead wraps around depending on how much samples there are.
		//So it will not overflow and this way the clip size is always small and is dependant entirely
		//on how long the mic is allowed to record for before looping. I.E. recording length. See streamMicrophoneAudio();
		micAudio.clip.GetData(soundData, readHead); //Get data from after the last point it was read from.
		
		//Set the readhead since we've read the data above now.
		readHead = (readHead + sampleAmount) % micAudio.clip.samples;
	}
}

//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
// 
//public class SoundTest : MonoBehaviour 
//{
//   
//    // Public Variables
//    public AudioSource Audio;
//    public int numSamples = 64;
//    public GameObject abar;
//	public GameObject aSphere;
//	public int maxBalls   = 10000;
//	private List<GameObject> ballList;
//	public float tDelta = 0.01f;
//	
//    // Private Variables
//    float[] numberleft = new float[64];
//    float[] numberright = new float[64];
////    GameObject[] thebarsleft;
////    GameObject[] thebarsright;
//    float spacing;
//    float width;
//	bool hasLorenzed = false;
//	public float scale = 0.1f;
//	
//	public float x1 = 0.1f;
//	public float y1 = 0.0f;
//	public float z1 = 0;
//	
//	public float a1 = 10;
//	public float b1 = 28;
//	public float c1 = 8.0f / 3.0f;
//	
//	public float changeRight=1;
//	public float changeLeft=1;
//	
//	Color colour1 = Color.cyan;
//	Color colour2 = Color.green;
//	
//	public float bLimit;
//	public float previousB = 0;
//	public float colourDifference = 0.2f;
//	
//	public float hardB = 50;
//	
//	public Color red;
//	public Color blue;
//	public Color green;
//	public float colourScale = 1;
//	public float dt;
//	private int colourCount;
//	public float rotScale = 1;
//	
//	public float lorenzScale = 5;
//	
//	public LineRenderer lineRenderer;
//	
//	Vector3 rotateVector;
//   
//    // Use this for initialization
//    void Start () 
//	{
////        thebarsleft = new GameObject[numSamples];
////        thebarsright = new GameObject[numSamples];
//        spacing = 0.4f - (numSamples * 0.001f);
//        width = 0.3f - (numSamples * 0.001f);
////        for(int i=0; i < numSamples; i++){
////            float xpos = i*spacing -8.0f;
////            Vector3 positionleft = new Vector3(xpos,3, 0);
////                thebarsleft[i] = (GameObject)Instantiate(abar, positionleft, Quaternion.identity) as GameObject;
////                thebarsleft[i].transform.localScale = new Vector3(width,1,0.2f);
////           
////            Vector3 positionright = new Vector3(xpos,-3, 0);
////                thebarsright[i] = (GameObject)Instantiate(abar, positionright, Quaternion.identity) as GameObject; 
////                thebarsright[i].transform.localScale = new Vector3(width,1,0.2f);
////        }
//		
//		//ballList = new List<GameObject>();
//		lineRenderer.SetVertexCount(maxBalls);
//		Material mat = new Material(Shader.Find("Particles/Additive"));
//		mat.color = new Color(1, 0.1f, 0.1f, 0.2f);
//		
//		colour1.a = 0.3f;
//		colour2.a = 0.3f;
//		
//		lineRenderer.material = mat;
//		lineRenderer.SetColors(colour1, colour2);
//		//Debug.Log("Colour is: "+lineRenderer.material.color);
//    }
//	
//	void lerpColours()
//	{
//		if(colourCount==0)
//		{
//			colour1 = Color.Lerp(red, green, dt*colourScale);
//			
//		}
//		else if(colourCount==1)
//		{
//			colour1 = Color.Lerp(green, blue, dt*colourScale);
//		}
//		else if(colourCount==2)
//		{
//			colour1 = Color.Lerp(blue, red, dt*colourScale);
//		}
//		
//		colour2 = colour1;
//		colour2.r+=colourDifference;
//		colour2.g+=colourDifference;
//		colour2.b+=colourDifference;
//		
//		dt+=Time.deltaTime;
//		
//		if(dt>1/colourScale)
//		{
//			dt=0;
//			colourCount++;
//			if(colourCount>2)
//			{
//				colourCount=0;
//			}
//		}
//		
//		lineRenderer.SetColors(colour1, colour2);
//	}
//   
//    // Update is called once per frame
//    void Update () 
//	{
//       
//        audio.GetOutputData(numberleft, 0);
//        audio.GetOutputData(numberright, 1);
//       
//        for(int i=0; i < numSamples; i++)
//		{
//            if (float.IsInfinity(numberleft[i]*30) || float.IsNaN(numberleft[i]*30))
//			{
//            }else
//			{
//               // thebarsleft[i].transform.localScale = new Vector3(width, numberleft[i]*30,0.2f);
//                //thebarsright[i].transform.localScale = new Vector3(width, numberright[i]*30,0.2f); 
//				
//				changeLeft = Mathf.Abs(numberleft[i]);
//				changeRight =  Mathf.Abs(numberright[i]);
//				
//				//changeLeft = numberleft[i]+0.6f;
//				//changeRight = numberright[i]+0.6f;
//            }
//        }
////		if(!hasLorenzed)
////		{
////			hasLorenzed = true;
////			lorenz();
////		}
////		else
////		{
////			hasLorenzed=false;
////		}
//		
//		lorenz();
//		
//		if(Input.GetKeyDown(KeyCode.A))
//		{
//			Debug.Log("Colour is: "+lineRenderer.material.color);
//		}
//		
//		lerpColours();
//		
//		transform.RotateAround(new Vector3(0, 0, 40), new Vector3(1, 1, 0), Time.deltaTime*rotScale);
//		
//		
//	}
//	
//	void lorenz()
//	{
//		tDelta=100f/maxBalls;
//		
//		int numBalls;
//		
//		float x = x1;
//		float y = y1;
//		float z = z1;
//		
//		float a = a1;
//		//float b = b1;
//		
//		//limit b change
//		float bChange = lorenzScale*(changeLeft+changeRight+0.1f);
//		bChange-=previousB;
//		
//		if(bChange>bLimit)
//		{
//			bChange = bLimit;
//		}
//		
//		if(bChange<-bLimit)
//		{
//			bChange =-bLimit;
//		}
//		
//		float b = bChange+previousB;
//		float c = c1;
//		
//		previousB = b;
//	
//		float xNew, yNew, zNew;
//		  
//		for(numBalls=0;numBalls<maxBalls;numBalls++) 
//		{
//			
//			xNew = x + a*(y-x)*tDelta;
//			yNew = y + (x*(b-z)-y)*tDelta;
//			zNew = z + (x*y-c*z)*tDelta;
//			
//			Vector3 pos = new Vector3(xNew, yNew, zNew);
//			
//			lineRenderer.SetPosition(numBalls, pos*scale);
//			
//			x=xNew;
//			y=yNew;
//			z=zNew;
//			
//			if(numBalls==maxBalls/2)
//			{
//				rotateVector=pos*scale;
//			}
//		}
//	}
//}
using UnityEngine;
using System.Collections;

public class ContentCycler : MonoBehaviour {
	
	public GameObject offButton;
	public GameObject[] markerContent;
	public AudioSource touchClickSound;
	private int contentIndex = 0, max = 0;
	private int[] availableNumbers;
	private VideoPlaybackBehaviour videoContent;
	
	void OnEnable()
	{
		offButton.SetActive(true);
	}
	
	// Use this for initialization
	void Start () 
	{
		max = markerContent.Length-1;
		availableNumbers = new int[markerContent.Length];
		resetMaxNumbers();
		
		findVideos();
		changeContent();
	}
	
	// Update is called once per frame
	void Update () 
	{
		controls();
		if(Input.GetKeyDown(KeyCode.Return))
		{
			changeContent();
		}
	}
	
	private void controls()
	{
		if(Input.touchCount > 0)
		{
			Touch touch = Input.touches[0];
			
			if(touch.phase == TouchPhase.Began)
			{
				changeContent();
			}
		}
	}
	
	private void changeContent()
	{
		touchClickSound.Play();
		contentIndex = getNewRandomNum();
		
		for(int i = 0; i < markerContent.Length; ++i)
		{
			markerContent[i].SetActive(false);
		}
		
		markerContent[contentIndex].SetActive(true);
		
		if(markerContent[contentIndex].name == "Video")
		{
			if(videoContent.VideoPlayer != null)
			{
				if(videoContent.VideoPlayer.GetStatus() == VideoPlayerHelper.MediaState.REACHED_END)
				{
					videoContent.VideoPlayer.Play(false, 0);
				}
					
				videoContent.VideoPlayer.Play(false, videoContent.VideoPlayer.GetCurrentPosition());
			}
		}
		else
		{
			if(videoContent.VideoPlayer != null)
			{
				videoContent.VideoPlayer.Pause();
			}
		}
	}
	
	private int getNewRandomNum() //Makes sure a unique number is randomised without using a loop. Google Fisher-Yates shuffle
	{
		int i = Random.Range(0, max);
		int tempR = 0;
		int tempM = 0;
		int randomNumber = availableNumbers[i];
		
		tempR = availableNumbers[i];
		tempM = availableNumbers[max];
		
		availableNumbers[i] = tempM;
		availableNumbers[max] = tempR;
		
		--max;
		
		if(max == 0)
		{
			resetMaxNumbers();
		}
		
		return randomNumber;
	}
	
	private void resetMaxNumbers()
	{
		for(int i = 0; i < availableNumbers.Length; ++i)
		{
			availableNumbers[i] = i; //reset the values from 0 - max
		}
		
		max = markerContent.Length - 1;
	}
	
	private void findVideos()
	{
		for(int i = 0; i < markerContent.Length; ++i)
		{
			if(markerContent[i].GetComponent<VideoPlaybackBehaviour>() != null)
			{
				videoContent = markerContent[i].GetComponent<VideoPlaybackBehaviour>();
			}
		}
	}
}

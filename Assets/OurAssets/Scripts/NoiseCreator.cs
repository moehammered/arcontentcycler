using UnityEngine;
using System.Collections;

public class NoiseCreator : MonoBehaviour {
	
	public Texture2D[] noiseTexture;
	public int pixelWidth, pixelHeight;
	public float xOrigin, yOrigin;
	public float scale;
	public float transparency;
	public bool preSampleNoise = false;
	public int noiseSampleSize;
	
	private Color[] pixels;
	private float[] samples;
	
	// Use this for initialization
	void Start () 
	{
		if(pixelHeight <= 0 && pixelWidth <= 0)
		{
			Debug.Log("You set pixel dimensions wrong!");
		}
		else
		{
			if(noiseTexture.Length == 0)
			{
				noiseTexture = new Texture2D[1];
				noiseTexture[0] = new Texture2D(pixelWidth, pixelHeight);
			}
			else
			{
				for(int i = 0; i < noiseTexture.Length; ++i)
				{
					noiseTexture[i] = new Texture2D(pixelWidth, pixelHeight);
				}
			}
			
			pixels = new Color[pixelWidth*pixelHeight];
			
			renderer.material.mainTexture = noiseTexture[0];
		}
		
		initialisePixels();
		
		if(noiseTexture.Length != 0)
		{
			createNoiseTextures();
		}
		else if(preSampleNoise)
			precalculateNoise();
		else
			createNewNoise();
	}
	
	private void createNoiseTextures()
	{
		if(noiseTexture.Length == 0)
		{
			Debug.Log("Texture array is empty");
		}
		else
		{
			float xCoord, yCoord;
		
			if(pixels.Length != 0)
			{
				for(int i = 0; i < noiseTexture.Length; ++i)
				{
					for(int y = 0; y < noiseTexture[i].height; ++y)
					{
						for(int x = 0; x < noiseTexture[i].width; ++x)
						{
							xCoord = xOrigin + (float)x/(float)noiseTexture[i].width * scale;
							yCoord = yOrigin + (float)y/(float)noiseTexture[i].height * scale;
							float sample = Mathf.PerlinNoise(xCoord*Random.Range(1, Time.time), yCoord*Time.time);
							
							//pixels[y*noiseTexture.width + x] = new Color(sample, sample, sample, transparency);
							pixels[y*noiseTexture[i].width + x].r = sample;
							pixels[y*noiseTexture[i].width + x].g = sample;
							pixels[y*noiseTexture[i].width + x].b = sample;
						}
					}
					
					noiseTexture[i].SetPixels(pixels);
					noiseTexture[i].Apply();
				}
				
			}
		}
	}
	
	private void initialisePixels()
	{
		for(int y = 0; y < noiseTexture[0].height; ++y)
		{
			for(int x = 0; x < noiseTexture[0].width; ++x)
			{
				pixels[y*noiseTexture[0].width + x] = new Color(1, 1, 1, transparency);
			}
		}
	}
	
	private void precalculateNoise()
	{
		samples = new float[noiseSampleSize];
		
		for(int i = 0; i < samples.Length; ++i)
		{
			samples[i] = Mathf.PerlinNoise(scale * Time.time, 0);
		}
	}
	
	private void createNewNoise()
	{
		float xCoord, yCoord;
		
		if(pixels.Length != 0)
		{
			for(int y = 0; y < noiseTexture[0].height; ++y)
			{
				for(int x = 0; x < noiseTexture[0].width; ++x)
				{
					xCoord = xOrigin + (float)x/(float)noiseTexture[0].width * scale;
					yCoord = yOrigin + (float)y/(float)noiseTexture[0].height * scale;
					float sample = Mathf.PerlinNoise(xCoord*Random.Range(1, Time.time), yCoord*Time.time);
					
					//pixels[y*noiseTexture.width + x] = new Color(sample, sample, sample, transparency);
					pixels[y*noiseTexture[0].width + x].r = sample;
					pixels[y*noiseTexture[0].width + x].g = sample;
					pixels[y*noiseTexture[0].width + x].b = sample;
				}
			}
			
			setTextuePixels();
		}
	}
	
	private void getSampledNoise()
	{
		for(int y = 0; y < noiseTexture[0].height; ++y)
		{
			for(int x = 0; x < noiseTexture[0].width; ++x)
			{
				int sampleIdx = Random.Range(0, samples.Length-1);
				float sample = samples[sampleIdx];
				//pixels[y*noiseTexture.width + x] = new Color(sample, sample, sample, transparency);
				pixels[y*noiseTexture[0].width + x].r = sample;
				pixels[y*noiseTexture[0].width + x].g = sample;
				pixels[y*noiseTexture[0].width + x].b = sample;
			}
		}
	}
	
	private void setTextuePixels()
	{
		noiseTexture[0].SetPixels(pixels);
		noiseTexture[0].Apply();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void FixedUpdate()
	{
		if(noiseTexture.Length > 1)
		{
			renderer.material.mainTexture = noiseTexture[Random.Range(0, noiseTexture.Length-1)];
		}
		else if(preSampleNoise)
			getSampledNoise();
		else
			createNewNoise();
	}
}

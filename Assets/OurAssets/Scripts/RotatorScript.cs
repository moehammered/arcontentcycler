using UnityEngine;
using System.Collections;

public class RotatorScript : MonoBehaviour {
	
	public float rotationSpeed = 10.0f;
	public Vector3 axis;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.Rotate(axis, rotationSpeed);
	}
}

using UnityEngine;
using System.Collections;

public class LorenzVisualiser : MonoBehaviour {
	
	public float /*stepsPerPoint,*/ scale;
	public float sigma, rho, beta;
	public int pointCount;
	public bool pointByPoint, useModifier, useMusic;
	public float modifier, highestModValue, lowestModValue;
	public float x, y, z;
	public float lorenzScale = 5;
	public AudioSource audioMusic;
	
	private float xNew, yNew, zNew;	
	private int currentPoints;
	public float delta;
	private float[] samples;
	private LineRenderer line;

	// Use this for initialization
	void Start () {
		samples = new float[1024];
		if(audioMusic == null)
		{
			audioMusic = gameObject.GetComponent<AudioSource>();
		}
		line = gameObject.GetComponent<LineRenderer>();
		line.SetVertexCount(pointCount);
	}
	
	private void resetPoints()
	{
		x = y = z = delta;
		currentPoints = 0;
	}
	
	private void controls()
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			resetPoints();
		}
		if(Input.GetKeyDown(KeyCode.Q))
		{
			sigma -= 1;
		}
		if(Input.GetKeyDown(KeyCode.W))
		{
			sigma += 1;
		}
		if(Input.GetKeyDown(KeyCode.E))
		{
			rho -= 1;
		}
		if(Input.GetKeyDown(KeyCode.R))
		{
			rho += 1;
		}
		if(Input.GetKeyDown(KeyCode.T))
		{
			beta -= 1;
		}
		if(Input.GetKeyDown(KeyCode.Y))
		{
			beta += 1;
		}
		
		if (Input.GetAxis("Mouse ScrollWheel") < 0)
		{
			Camera.mainCamera.transform.Translate(0, 0, -1);
		}
		if (Input.GetAxis("Mouse ScrollWheel") > 0)
		{
			Camera.mainCamera.transform.Translate(0, 0, 1);
		}
		if(Input.GetKey(KeyCode.LeftArrow))
		{
			Camera.mainCamera.transform.Translate(-1.0f, 0, 0);
		}
		if(Input.GetKey(KeyCode.RightArrow))
		{
			Camera.mainCamera.transform.Translate(1.0f, 0, 0);
		}
		if(Input.GetKey(KeyCode.UpArrow))
		{
			Camera.mainCamera.transform.Translate(0, 1.0f, 0);
		}
		if(Input.GetKey(KeyCode.DownArrow))
		{
			Camera.mainCamera.transform.Translate(0, -1.0f, 0);
		}
	}
	
	private void proceduralCalculation()
	{
		if(currentPoints < pointCount)
		{
			//logValues(currentPoints);
			xNew = calculateDeltaX();
			yNew = calculateDeltaY();
			zNew = calculateDeltaZ();
			
			x = xNew;
			y = yNew;
			z = zNew;
			
			line.SetPosition(currentPoints, new Vector3(x * scale, y * scale, z * scale));
			
			//logValues(currentPoints);
			++currentPoints;
		}
	}
	
	private void getSoundData()
	{
		modifier = 0.0f;
		if(audioMusic.isPlaying)
		{
			audioMusic.GetOutputData(samples, 0);
			for(int i = 0; i < samples.Length; ++i)
			{
				modifier += samples[i];
			}
			if(modifier < -100)
			{
				modifier = -100;
			}
			if(modifier > 100)
			{
				modifier = 100;
			}
			
			if((int)modifier > (int)highestModValue)
			{
				highestModValue = modifier;
			}
			else if((int)modifier < (int)lowestModValue)
			{
				lowestModValue = modifier;
			}
			
			modifier /= 10.0f;
//			if(modifier < 0)
//			{
//				modifier *= -1;
//			}
			
			if(currentPoints == pointCount-1)
			{
				resetPoints();
			}
		}
	}
	
	private void calculateVerts()
	{
		for(int i = currentPoints; i < pointCount ; ++i)
		{
			if(!useMusic)
			{
				xNew = calculateDeltaX();
				yNew = calculateDeltaY();
				zNew = calculateDeltaZ();
			}
			else
			{
				xNew = calculateDeltaX((int)(modifier*lorenzScale));
				yNew = calculateDeltaY((int)(modifier*lorenzScale));
				zNew = calculateDeltaZ();
			}

			x = xNew;
			y = yNew;
			z = zNew;
			
			line.SetPosition(i, new Vector3(x * scale, y * scale, z * scale));
			
			currentPoints = i;
		}
	}
	
	private void logValues(int currentPoints)
	{
		Debug.Log ("X,Y,Z = (" + x + ", " + y + ", " + z + ")" + " CurrentPoint: " + currentPoints);
		//Debug.Log("Sigma:" + sigma + " Rho:" + rho + " Beta:" + beta + " Delta:" + delta + " Modifier:" + modifier);
	}
	
	// Update is called once per frame
	void Update () {
			controls();
		if(!pointByPoint)
		{
			getSoundData();
			calculateVerts();
		}
		else
			proceduralCalculation();
	}
	
	private float calculateDeltaX()
	{
		return x + sigma * (y - x) * delta;
	}
	
	private float calculateDeltaY()
	{
		return y + (x *(rho - z) - y) * delta;
	}
	
	private float calculateDeltaZ()
	{
		return z + (x * y - beta*z) * delta;
	}
	
	private float calculateDeltaX(int mod)
	{
		return x + (sigma+mod) * (y - x) * delta;
	}
	
	private float calculateDeltaY(int mod)
	{
		return y + (x *((rho+mod) - z) - y) * delta;
	}
	
	private float calculateDeltaZ(int mod)
	{
		return z + (x * y - (beta+mod)*z) * delta;
	}
}

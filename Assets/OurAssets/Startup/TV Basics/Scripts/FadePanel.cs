using UnityEngine;
using System.Collections;

public class FadePanel : MonoBehaviour {
	
	public AudioSource soundToPlay;
	public GameObject planeToFade;
	public float fadeTimer = 5.0f;
	public float transparency = 0;
	public float transDelta = 0;
	public bool fadeIn = false;
	public bool finished = false;
	
	private Texture2D matText;
	private Color[] pixels;
//	private float timer = 0;
	// Use this for initialization
	void Start () 
	{
		if(soundToPlay != null)
			soundToPlay.Play();
		matText = new Texture2D(256, 256);
		
		pixels = matText.GetPixels();
		
		if(fadeIn)
		{
			transparency = 0;
		}
		else
		{
			transparency = 1;
		}
		
		for(int y = 0; y < matText.height;++y)
		{
			for(int x = 0; x < matText.width; ++x)
			{
				pixels[y*matText.width + x].a = transparency;
			}
		}
		
		matText.SetPixels(pixels);
		matText.Apply();
		
		transDelta = 1.0f/fadeTimer;
		planeToFade.renderer.material.mainTexture = matText;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!finished)
		{
			if(fadeIn)
			{
				fadeInAnim();
			}
			else
			{
				fadeOutAnim();
			}
			planeToFade.renderer.material.mainTexture = matText;
		}
	}
	
	private void fadeOutAnim()
	{
		if(transparency > 0.0f)
		{
			transparency -= Time.deltaTime * transDelta;
			
			for(int y = 0; y < matText.height;++y)
			{
				for(int x = 0; x < matText.width; ++x)
				{
					pixels[y*matText.width + x].a = transparency;
				}
			}
			
			matText.SetPixels(pixels);
			matText.Apply();
		}
		else
		{
			finished = true;
		}
	}
	
	private void fadeInAnim()
	{
		if(transparency < 1.0f)
		{
			transparency += Time.deltaTime * transDelta;
			
			for(int y = 0; y < matText.height;++y)
			{
				for(int x = 0; x < matText.width; ++x)
				{
					pixels[y*matText.width + x].a = transparency;
				}
			}
			
			matText.SetPixels(pixels);
			matText.Apply();
		}
		else
		{
			finished = true;
		}
	}
}

using UnityEngine;
using System.Collections;

public class PopLetters : MonoBehaviour {
	
	public GameObject[] letters;
	public AnimationController animToActivate;
	public float animTime = 5.0f;
	public bool finished = false;
	public FadePanel backPanel;
	
	private int currLetter = 0;
	private float frameDelta = 0;
	private float timer = 0;
	// Use this for initialization
	void Start () 
	{
		frameDelta = 1.0f/letters.Length;
		if(backPanel == null)
		{
			print ("how is it null?");
		}
		currLetter = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(backPanel == null)
		{
			backPanel = GameObject.Find("TV_Frame").GetComponent<FadePanel>();
		}
		else if(backPanel.finished && !finished)
		{
			animToActivate.enabled = true;
			checkLetters();
//			timer += Time.deltaTime;
//			
//			if(timer > frameDelta)
//			{
//				letters[currLetter].SetActive(true);
//				
//				currLetter++;
//				
//				if(currLetter == letters.Length)
//				{
//					finished = true;
//				}
//				timer = 0;
//			}
		}
		else if(finished)
		{
			this.enabled = false;
		}
	}
	
	private void checkLetters()
	{
		letters[currLetter].SetActive(true);
		if(animToActivate.animationsEnded[currLetter])
		{
			currLetter++;
			if(currLetter == letters.Length)
			{
				finished = true;
				currLetter = 0;
			}
		}
	}
}

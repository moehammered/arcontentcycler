using UnityEngine;
using System.Collections;

public class FadeLogo : MonoBehaviour {
	
	private AnimationController anim;
	public GameObject letter, logo, contentCycler;
	public float fadeTimer = 5.0f;
	public float transparency = 0;
	public float transDelta = 0;
	public bool fadeIn = false;
	public bool finished = false;
	
	private Texture2D matText;
	private Color[] pixels;
//	private float timer = 0;
	// Use this for initialization
	void Start () {
		anim = gameObject.GetComponent<AnimationController>();
		matText = new Texture2D(256, 256);
		
		pixels = matText.GetPixels();
		
		if(fadeIn)
		{
			transparency = 0;
		}
		else
		{
			transparency = 1;
		}
		
		for(int y = 0; y < matText.height;++y)
		{
			for(int x = 0; x < matText.width; ++x)
			{
				pixels[y*matText.width + x].r = 1;
				pixels[y*matText.width + x].g = 1;
				pixels[y*matText.width + x].b = 1;
				pixels[y*matText.width + x].a = transparency;
			}
		}
		
		matText.SetPixels(pixels);
		matText.Apply();
		
		transDelta = 1.0f/fadeTimer;
		letter.renderer.sharedMaterial.mainTexture = matText;
	}
	
	void Update () 
	{
		if(anim.animEnded)
		{
			if(!finished)
			{
				if(fadeIn)
				{
					fadeInAnim();
				}
				else
				{
					fadeOutAnim();
				}
				letter.renderer.sharedMaterial.mainTexture = matText;
			}
			else
			{
				GameObject.Destroy(logo, 0.5f);
				contentCycler.SetActive(true);
				this.enabled = false;
			}
		}
		else if(finished)
		{
			GameObject.Destroy(logo, 0.5f);
			contentCycler.SetActive(true);
			this.enabled = false;
		}
	}
	
	private void fadeOutAnim()
	{
		if(transparency > 0.0f)
		{
			transparency -= Time.deltaTime * transDelta;
			
			for(int y = 0; y < matText.height;++y)
			{
				for(int x = 0; x < matText.width; ++x)
				{
					pixels[y*matText.width + x].a = transparency;
				}
			}
			
			matText.SetPixels(pixels);
			matText.Apply();
		}
		else
		{
			finished = true;
		}
	}
	
	private void fadeInAnim()
	{
		if(transparency < 1.0f)
		{
			transparency += Time.deltaTime * transDelta;
			
			for(int y = 0; y < matText.height;++y)
			{
				for(int x = 0; x < matText.width; ++x)
				{
					pixels[y*matText.width + x].a = transparency;
				}
			}
			
			matText.SetPixels(pixels);
			matText.Apply();
		}
		else
		{
			finished = true;
		}
	}
}

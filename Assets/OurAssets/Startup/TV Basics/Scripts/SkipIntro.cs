using UnityEngine;
using System.Collections;

public class SkipIntro : MonoBehaviour {
	
	public AudioSource soundToStop;
	public FadeLogo contentStarter;
	public float doubleTapTimer = 0.5f;
	public bool canDoubleTap = false;
	public bool enableTimer = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		checkControls();
		checkDoubleTap();
	}
	
	private void checkControls()
	{
		if(Input.touchCount > 0)
		{
			Touch touch = Input.touches[0];
			
			if(touch.phase == TouchPhase.Began && !enableTimer)
			{
				startTimer();
			}
			else if(touch.phase == TouchPhase.Began && canDoubleTap)
			{
				audio.Stop();
				contentStarter.finished = true;
				this.enabled = false;
			}
		}
		if(Input.GetKeyDown(KeyCode.Return))
		{
			if(!enableTimer)
			{
				startTimer();
			}
			else if(canDoubleTap)
			{
				soundToStop.Stop();
				contentStarter.finished = true;
				this.enabled = false;
			}
		}
	}
	
	private void startTimer()
	{
		enableTimer = true;
	}
	
	private void checkDoubleTap()
	{
		canDoubleTap = false;
		if(enableTimer)
		{
			doubleTapTimer -= Time.deltaTime;
			if(doubleTapTimer > 0)
			{
				canDoubleTap = true;
			}
			else
			{
				enableTimer = false;
				doubleTapTimer = 0.5f;
			}
		}
	}
}

using UnityEngine;
using System.Collections;

public class ActivateAnim : MonoBehaviour {
	
	public AnimationController anim;
	public GameObject logo;
	public bool finished = false;
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () {
		if(logo.GetComponent<PopLetters>().finished)
		{
			anim.enabled = true;
			this.enabled = false;
		}
	}
}

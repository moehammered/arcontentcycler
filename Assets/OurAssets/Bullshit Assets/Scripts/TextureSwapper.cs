using UnityEngine;
using System.Collections;

public class TextureSwapper : MonoBehaviour {
	
	public GameObject planeObject;
	public Texture[] textures;
	public float timePerFrame = 0.1f;
	
	private float timer;
	private int currTexture = 0;
	// Use this for initialization
	void Start () {
		currTexture = 0;
		
		planeObject.renderer.material.mainTexture = textures[currTexture];
	}
	
	// Update is called once per frame
	void Update () 
	{
		timer += Time.deltaTime;
		
		if(timer > timePerFrame)
		{
			currTexture++;
			
			if(currTexture == textures.Length)
			{
				currTexture = 0;
			}
			
			planeObject.renderer.material.mainTexture = textures[currTexture];
		}
	
	}
}

using UnityEngine;
using System.Collections;

public class VideoController : MonoBehaviour {
	
	public VideoPlaybackBehaviour video;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		checkVideo();
	}
	
	void OnDisable()
	{
		if(video.VideoPlayer != null)
		{
			if(video.VideoPlayer.GetStatus() == VideoPlayerHelper.MediaState.PLAYING)
			{
				video.VideoPlayer.Pause();
				video.VideoPlayer.SeekTo(0.0f);
			}
		}
		
		if(audio != null)
		{
			if(audio.isPlaying)
			{
				audio.Stop();
			}
		}
	}
	
	void OnEnable()
	{
		checkVideo();
		
		if(audio != null)
		{
			audio.Play();
			audio.loop = true;
		}
	}
	
	private void checkVideo()
	{
		if (video != null && video.VideoPlayer != null)
        {
            if (video.VideoPlayer.IsPlayableOnTexture())
            {
                // This video is playable on a texture, toggle playing/paused

                VideoPlayerHelper.MediaState state = video.VideoPlayer.GetStatus();
                if (state == VideoPlayerHelper.MediaState.PAUSED ||
                    state == VideoPlayerHelper.MediaState.READY ||
                    state == VideoPlayerHelper.MediaState.STOPPED)
                {
                    // Pause other videos before playing this one
                    PauseOtherVideos(video);

                    // Play this video on texture where it left off
                    video.VideoPlayer.Play(false, video.VideoPlayer.GetCurrentPosition());
                }
                else if (state == VideoPlayerHelper.MediaState.REACHED_END)
                {
                    // Pause other videos before playing this one
                    PauseOtherVideos(video);

                    // Play this video from the beginning
                    video.VideoPlayer.Play(false, 0);
                }
            }
		}
	}
	
	private void PauseOtherVideos(VideoPlaybackBehaviour currentVideo)
    {
        VideoPlaybackBehaviour[] videos = (VideoPlaybackBehaviour[])
                FindObjectsOfType(typeof(VideoPlaybackBehaviour));

        foreach (VideoPlaybackBehaviour video in videos)
        {
            if (video != currentVideo)
            {
                if (video.CurrentState == VideoPlayerHelper.MediaState.PLAYING)
                {
                    video.VideoPlayer.Pause();
                }
            }
        }
    }
}
